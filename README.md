# Net Project

## TOC

- [Intro](#Intro)  
- [Features](#Features)  
- [Tech](#Tech)  
- [Prep](#Prep)  
- [Standard](#Standard)  
- [Localhost](#Localhost)
- [Others](#Others)  
- [Docs]()

## Tech
The project is built using ASP.NET CORE 8.0, EF Core, Serilog, Hangfire, SignalR, ELK etc<br/>
Recommended IDE for the project is [Microsoft Visual Studio 2022 Community Edition](https://visualstudio.microsoft.com/downloads/), and install its open-source edition <br/>
Other tech recommendations:
1. SourceTree - for Git
2. SQL Server Management Studio 19 - for DB
3. Postman - testing API
4. Notepad++, TortoiseGit, WinMerge

## Standard
Following are the code standard:
folderName : PascalCase    
variable   : PascalCase (public) | CamelCase with prefix "_" (private)
className  : PascalCase 

## Localhost
- Install docker, redis
- testing url: https://localhost:4001/swagger/index.html (Starts automatically when the start button is triggered)