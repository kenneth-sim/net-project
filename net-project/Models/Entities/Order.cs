﻿using net_project.Services.Interfaces;
using System.ComponentModel.DataAnnotations;

namespace net_project.Models.Entities
{
    public class Order : IAuditableEntity
    {
        [Key]
        public int Id { get; set; }
        public decimal PlaceAmount { get; set; }
        public decimal WalletAmount { get; set; }
        public decimal ReturnAmount { get; set; }
        public string Status { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public string UserId { get; set; }
        public int GameId { get; set; }
        public User User {  get; set; }
        public Game Game { get; set; }
        public ICollection<OrderItem> OrderItems { get; set; }
        public ICollection<Transaction> Transactions { get; set; }
    }
}
