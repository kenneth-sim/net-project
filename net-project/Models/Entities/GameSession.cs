﻿using System.ComponentModel.DataAnnotations;

namespace net_project.Models.Entities
{
    public class GameSession
    {
        [Key]
        public int Id { get; set; }
        public int GameId { get; set; }
        public Game Game { get; set; }
    }
}
