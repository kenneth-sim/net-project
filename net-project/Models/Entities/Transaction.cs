﻿using net_project.Services.Interfaces;
using System.ComponentModel.DataAnnotations;

namespace net_project.Models.Entities
{
    public class Transaction : IAuditableEntity
    {
        [Key]
        public int Id { get; set; }
        public string Type { get; set; }
        public decimal Amount { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public int? OrderId { get; set; }
        public int WalletId { get; set; }
        public Wallet Wallet { get; set; }
        public Order? Order { get; set; }
    }
}
