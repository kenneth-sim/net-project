﻿namespace net_project.Models.Error
{
    public class ErrorCodeException : Exception
    {
        public ErrorCode ErrorCode { get; }

        public ErrorCodeException(ErrorCode errorCode)
            : base(errorCode.GetMessage())
        {
            ErrorCode = errorCode;
        }

        public ErrorCodeException(ErrorCode errorCode, string addMessage)
          : base(errorCode.GetMessage() + " " + addMessage)
        {
            ErrorCode = errorCode;
        }
    }
}
