﻿using System.Reflection;

namespace net_project.Models.Error
{
    public enum ErrorCode
    {
        [ErrorMessage(10001, "Failed to login.")]
        ERROR_LOGIN_FAILED,
        [ErrorMessage(10002, "Password given is invalid.")]
        ERROR_INVALID_PASSWORD,
        [ErrorMessage(10003, "Failed to register.")]
        ERROR_REGISTERED_FAILED,
        [ErrorMessage(10004, "Unauthorized access.")]
        ERROR_UNAUTHORISED,
        [ErrorMessage(10005, "Sign Mismatch.")]
        ERROR_SIGN_MISMATCH,

        [ErrorMessage(20001, "Username cannot be null.")]
        VALIDATE_USERNAME,
        [ErrorMessage(20002, "Password cannot be null.")]
        VALIDATE_PASSWORD,

        [ErrorMessage(30001, "User cannot be found.")]
        NOTFOUND_USER,
        [ErrorMessage(30002, "Role cannot be found.")]
        NOTFOUND_ROLE,
        [ErrorMessage(30003, "Announcement cannot be found.")]
        NOTFOUND_ANNOUNCEMENT,

        [ErrorMessage(50001, "SQL Server connection failed.")]
        CONNECTTION_SQLSERVER,
        [ErrorMessage(50002, "Redis connection failed.")]
        CONNECTTION_REDIS,

        [ErrorMessage(40001, "Announcement is existed.")]
        EXISTED_ANNOUNCEMENT,

        [ErrorMessage(99999, "Unknown Exception.")]
        EXCEPTION_THROWN
    }

    [AttributeUsage(AttributeTargets.Field, Inherited = false, AllowMultiple = false)]
    sealed class ErrorMessageAttribute : Attribute
    {
        public int StatusCode { get; }
        public string Message { get; }

        public ErrorMessageAttribute(int statusCode, string message)
        {
            StatusCode = statusCode;
            Message = message;
        }
    }

    public static class ErrorCodeExtensions
    {
        public static int GetStatusCode(this ErrorCode errorCode)
        {
            var attribute = GetErrorMessageAttribute(errorCode);
            return attribute?.StatusCode ?? 0;
        }

        public static string GetMessage(this ErrorCode errorCode)
        {
            var attribute = GetErrorMessageAttribute(errorCode);
            return attribute?.Message ?? "";
        }

        private static ErrorMessageAttribute? GetErrorMessageAttribute(ErrorCode errorCode)
        {
            var fieldInfo = errorCode.GetType().GetField(errorCode.ToString());
            return fieldInfo?.GetCustomAttribute<ErrorMessageAttribute>();
        }

        public static ErrorCode GetValidationErrorCode(String field)
        {
            var errorCodeName = "VALIDATE_" + field.ToUpper();
            return Enum.GetValues(typeof(ErrorCode))
                .Cast<ErrorCode>()
                .Where(ec => ec.ToString().Equals(errorCodeName))
                .FirstOrDefault();
        }
    }

}
