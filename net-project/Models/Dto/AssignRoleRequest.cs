﻿namespace net_project.Models.Dto
{
    public class AssignRoleRequest
    {
        public string Username { get; set; }
        public string RoleName { get; set; }
    }
}
