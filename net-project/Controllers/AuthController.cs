﻿using AuthService.Models.Dto;
using net_project.Models.Dto;
using net_project.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace net_project.Controllers
{
    [Route("[controller]")]
    [ApiController]
    
    public class AuthController : ControllerBase
    {
        private readonly IAuthService _authService;

        public AuthController(IAuthService authService)
        {
            _authService = authService;
        }

        [HttpPost("login")]
        public async Task<IActionResult> Login([FromBody] LoginRequest requestDto)
        {
            return Ok(new Response(await _authService.Login(requestDto)));
        }

        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody] RegistrationRequest requestDto)
        {
            await _authService.Register(requestDto);
            return Ok(new Response("User registered successfully."));
        }

        [HttpPost("logout")]
        public IActionResult Logout([FromQuery] string username)
        {
            _authService.Logout(username);
            return Ok(new Response("User logout successfully."));
        }

        [HttpPost("assign-role"), Authorize(Roles = "ROLE_ADMIN")]
        public async Task<IActionResult> AssignRole([FromBody] AssignRoleRequest assignRoleRequest)
        {
            await _authService.AssignRole(assignRoleRequest);
            return Ok(new Response("Role " + assignRoleRequest.RoleName + " assigned to " + assignRoleRequest.Username + " successfully."));
        }
    }
}
