using net_project.Data;
using net_project.Hubs;
using net_project.Interceptors;
using net_project.Middlewares;
using net_project.Models.Dto;
using net_project.Models.Entities;
using net_project.Models.Error;
using net_project.Services.Implementation;
using net_project.Services.Implementation.Clients;
using net_project.Services.Interfaces;
using Hangfire;
using HangfireBasicAuthenticationFilter;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Serilog;
using Serilog.Exceptions;
using Serilog.Sinks.Elasticsearch;
using System.Reflection;
using System.Text;
using Microsoft.Extensions.Options;
using Serilog.Templates;
using net_project.Middlewares.Filters;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddScoped<IAuthService, AuthServiceImpl>();
builder.Services.AddScoped<IJwtService, JwtServiceImpl>();
builder.Services.AddScoped<IAnnouncementService, AnnouncementServiceImpl>();
builder.Services.AddScoped<CoinMarketCapClient>();
builder.Services.AddScoped<ISchedulerService, SchedulerServiceImpl>();
builder.Services.AddScoped<IUserService, UserServiceImpl>();
builder.Services.AddScoped<UserManager<User>>();
builder.Services.AddSingleton<SecurityHashVerificationFilter>();
builder.Services.AddSingleton<AuditableEntityInterceptor>();

builder.Services.AddSignalR();

var cpServer = Environment.GetEnvironmentVariable("CP_SERVER") ?? builder.Configuration.GetValue<string>("AppSettings:CP_SERVER")!;
var cpName = Environment.GetEnvironmentVariable("CP_NAME") ?? builder.Configuration.GetValue<string>("AppSettings:CP_NAME")!;
var cpPwd = Environment.GetEnvironmentVariable("CP_PWD") ?? builder.Configuration.GetValue<string>("AppSettings:CP_PWD")!;

builder.Services.AddDbContext<AppDbContext>((sp, option) =>
{  
    var cpDb = Environment.GetEnvironmentVariable("CP_DB") ?? builder.Configuration.GetValue<string>("AppSettings:CP_DB")!;
    var connectionString = $"Server={cpServer};Database={cpDb};User ID={cpName};Password={cpPwd};TrustServerCertificate=true;";

    option.UseSqlServer(connectionString ?? throw new ErrorCodeException(ErrorCode.CONNECTTION_SQLSERVER))
    .AddInterceptors(sp.GetService<AuditableEntityInterceptor>()!);
});

builder.Services.AddHangfire(config =>
{
    var hangFireDb = Environment.GetEnvironmentVariable("HANGFIRE_DB") ?? builder.Configuration.GetValue<string>("AppSettings:HANGFIRE_DB")!;
    var connectionString = $"Server={cpServer};Database={hangFireDb};User ID={cpName};Password={cpPwd};TrustServerCertificate=true;";

    config.SetDataCompatibilityLevel(CompatibilityLevel.Version_180)
        .UseSimpleAssemblyNameTypeSerializer()
        .UseRecommendedSerializerSettings()
        .UseSqlServerStorage(connectionString);
});
builder.Services.AddHangfireServer();

builder.Services.AddStackExchangeRedisCache(option =>
{
    var redisHost = Environment.GetEnvironmentVariable("REDIS_HOST") ?? builder.Configuration.GetValue<string>("AppSettings:REDIS_HOST")!;
    var redisPort = Environment.GetEnvironmentVariable("REDIS_PORT") ?? builder.Configuration.GetValue<string>("AppSettings:REDIS_PORT")!;
    var redisPwd = !string.IsNullOrEmpty(Environment.GetEnvironmentVariable("REDIS_PWD")) ? $",password={Environment.GetEnvironmentVariable("REDIS_PWD")}" : "";
    var redisDb = !string.IsNullOrEmpty(Environment.GetEnvironmentVariable("REDIS_DB")) ? $",defaultDatabase={Environment.GetEnvironmentVariable("REDIS_DB")}" : "";

    var connectionString = $"{redisHost}:{redisPort}{redisPwd}{redisDb}";

    option.Configuration = connectionString ?? throw new ErrorCodeException(ErrorCode.CONNECTTION_REDIS);
});

builder.Services.AddIdentity<User, IdentityRole>()
    .AddEntityFrameworkStores<AppDbContext>();

builder.Services.AddControllers().ConfigureApiBehaviorOptions(options =>
{
    options.InvalidModelStateResponseFactory = context =>
    {
        return new ValidationFailedResponse(context.ModelState);
    };
});

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(option =>
{
    option.AddSecurityDefinition(name: JwtBearerDefaults.AuthenticationScheme, securityScheme: new OpenApiSecurityScheme
    {
        Name = "Authorization",
        Description = "Enter JWT Token with 'Bearer '",
        In = ParameterLocation.Header,
        Type = SecuritySchemeType.ApiKey,
        Scheme = JwtBearerDefaults.AuthenticationScheme
    });
    option.AddSecurityRequirement(new OpenApiSecurityRequirement {
    {
        new OpenApiSecurityScheme
        {
        Reference = new OpenApiReference
        {
            Type = ReferenceType.SecurityScheme,
            Id = JwtBearerDefaults.AuthenticationScheme
        }
        },
        new string[] { }
    }
    });
});

ConfigureLogging(builder);

var key = Encoding.UTF8.GetBytes(builder.Configuration.GetValue<string>("AppSettings:JWT_KEY")!);

builder.Services.AddAuthentication(
    options => {
    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
    options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
})
.AddJwtBearer(x =>
{
    x.TokenValidationParameters = new TokenValidationParameters
    {
        ValidateAudience = false,
        ValidateIssuer = false,
        ValidateIssuerSigningKey = true,
        IssuerSigningKey = new SymmetricSecurityKey(key),
        ValidateLifetime = true
    };
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHangfireDashboard("/hangfire", new DashboardOptions
{
    DisplayStorageConnectionString = false,
    Authorization = new[] { new HangfireCustomBasicAuthenticationFilter
        {
            User = Environment.GetEnvironmentVariable("HANGFIREDB_NAME") ?? "hangfire123",
            Pass = Environment.GetEnvironmentVariable("HANGFIREDB_PWD") ?? "hangfire123",
        }
    },
});

app.UseSerilogRequestLogging();

app.UseLoggingMiddleware();

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.MapHub<StreamingHub>("/streaming-hub");

//ConfigureCronJob();

ApplyMigration();

app.Run();

void ApplyMigration()
{
    using (var scope = app.Services.CreateScope())
    {
        var _db = scope.ServiceProvider.GetRequiredService<AppDbContext>();
        if(_db.Database.GetPendingMigrations().Count() > 0)
        {
            _db.Database.Migrate();
        }
    }
}

void ConfigureLogging(WebApplicationBuilder builder)
{
    var configuration = builder.Configuration;
    var elkHost = Environment.GetEnvironmentVariable("ELK_HOST") ?? configuration.GetValue<string>("AppSettings:ELK_HOST")!;
    var elkPort = Environment.GetEnvironmentVariable("ELK_PORT") ?? configuration.GetValue<string>("AppSettings:ELK_PORT")!;
    var elkName = !string.IsNullOrEmpty(Environment.GetEnvironmentVariable("ELK_NAME")) ? $"{Environment.GetEnvironmentVariable("ELK_NAME")}:" : "";
    var elkPwd = !string.IsNullOrEmpty(Environment.GetEnvironmentVariable("ELK_PWD")) ? $"{Environment.GetEnvironmentVariable("ELK_PWD")}@" : "";
    var elkUrl = $"http://{elkName}{elkPwd}{elkHost}:{elkPort}";
    var configureELK = new ElasticsearchSinkOptions(new Uri(elkUrl))
    {
        AutoRegisterTemplate = true,
        IndexFormat = $"{Assembly.GetExecutingAssembly().GetName().Name!.ToLower().Replace(".", "-")}-{DateTime.UtcNow:yyyy-MM}",
        NumberOfReplicas = 2,
        NumberOfShards = 1
    };

    Log.Logger = new LoggerConfiguration()
    .Enrich.FromLogContext()
    .Enrich.WithExceptionDetails()
    .WriteTo.Debug(outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss} {MachineName} [{ThreadId}] {Level:u3} {SourceContext} - {Message:lj}{NewLine}{Exception}")
    .WriteTo.Console(outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss} {MachineName} [{ThreadId}] {Level:u3} {SourceContext} - {Message:lj}{NewLine}{Exception}")
    .WriteTo.Elasticsearch(configureELK)
    .CreateLogger();

    builder.Host.UseSerilog();
}

void ConfigureCronJob()
{
    RecurringJob.AddOrUpdate<ISchedulerService>(x => x.SendMessageToEvent(), "* * * * *");
    RecurringJob.AddOrUpdate<ISchedulerService>(x => x.SendMessageToStock(), "* * * * *");
    RecurringJob.AddOrUpdate<ISchedulerService>(x => x.SendMessageToCrypto(), "* * * * *");
}
