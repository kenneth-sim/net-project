﻿using net_project.Models;
using net_project.Models.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace net_project.Data
{
    public class AppDbContext : IdentityDbContext<User>
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }

        public DbSet<Player> Players {  get; set; }
        public DbSet<Admin> Admins {  get; set; }
        public DbSet<Announcement> Announcements { get; set; }
        public DbSet<AuditLog> AuditLogs { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }
        public DbSet<Game> Games { get; set; }
        public DbSet<GameSession> GameSessions { get; set; }
        public DbSet<GameResult> GameResults { get; set; }
        public DbSet<Transaction> Transactions { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<User>().ToTable("Users");
            builder.Entity<IdentityRole>().ToTable("Roles");
            builder.Entity<IdentityUserRole<string>>().ToTable("UserRoles");

            builder.Entity<Wallet>().HasOne(w => w.User).WithMany(u => u.Wallets).HasForeignKey(w => w.UserId);
            builder.Entity<GameResult>().HasOne(g => g.Game).WithMany(g => g.GameResults).HasForeignKey(g => g.GameId);
            builder.Entity<GameSession>().HasOne(g => g.Game).WithMany(g => g.GameSessions).HasForeignKey(g => g.GameId);
            builder.Entity<Order>().HasOne(o => o.Game).WithMany(g => g.Orders).HasForeignKey(o => o.GameId);
            builder.Entity<Order>().HasOne(o => o.User).WithMany(g => g.Orders).HasForeignKey(o => o.UserId);
            builder.Entity<Transaction>().HasOne(t => t.Order).WithMany(o => o.Transactions).HasForeignKey(t => t.OrderId);
            builder.Entity<Transaction>().HasOne(t => t.Wallet).WithMany(w => w.Transactions).HasForeignKey(t => t.WalletId);


            builder.Entity<Wallet>().Property(w => w.Balance).HasPrecision(18, 2);
            builder.Entity<Order>().Property(o => o.PlaceAmount).HasPrecision(18, 2);
            builder.Entity<Order>().Property(o => o.WalletAmount).HasPrecision(18, 2);
            builder.Entity<Order>().Property(o => o.ReturnAmount).HasPrecision(18, 2);
            builder.Entity<OrderItem>().Property(o => o.PlaceAmount).HasPrecision(18, 2);
            builder.Entity<OrderItem>().Property(o => o.WalletAmount).HasPrecision(18, 2);
            builder.Entity<OrderItem>().Property(o => o.ReturnAmount).HasPrecision(18, 2);
            builder.Entity<Transaction>().Property(t => t.Amount).HasPrecision(18, 2);

            builder.Entity<IdentityRole>().HasData(new IdentityRole
            {
                Id = "1",
                Name = SD.RoleType.ROLE_PLAYER.ToString(),
                NormalizedName = SD.RoleType.ROLE_PLAYER.ToString()
            });

            builder.Entity<IdentityRole>().HasData(new IdentityRole
            {
                Id = "2",
                Name = SD.RoleType.ROLE_MERCHANT.ToString(),
                NormalizedName = SD.RoleType.ROLE_MERCHANT.ToString()
            });

            builder.Entity<IdentityRole>().HasData(new IdentityRole
            {
                Id = "3",
                Name = SD.RoleType.ROLE_ADMIN.ToString(),
                NormalizedName = SD.RoleType.ROLE_ADMIN.ToString()
            });
        }
    }
}
