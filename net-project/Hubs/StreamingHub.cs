﻿using net_project.Models.Entities;
using net_project.Services.Interfaces;
using Microsoft.AspNetCore.SignalR;
using static net_project.Models.SD;

namespace net_project.Hubs
{
    public sealed class StreamingHub : Hub
    {
        private readonly IJwtService _jwtService;
        private readonly IUserService _userService;

        public StreamingHub(IJwtService jwtService, IUserService userService)
        {
            _jwtService = jwtService;
            _userService = userService;
        }

        public override async Task OnConnectedAsync()
        {
            Context.GetHttpContext()!.Request.Query.TryGetValue("access_token", out var jwtToken);

            if(string.IsNullOrEmpty(jwtToken))
            {
                Context.Abort();
                return;
            }

            string? userName = _jwtService.ExtractUsername(jwtToken);

            if (string.IsNullOrEmpty(userName))
            {
                Context.Abort();
                return;
            }

            User user = _userService.GetUserByUserName(userName);

            if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(user.Jwt))
            {
                Context.Abort();
                return;
            }

            await Groups.AddToGroupAsync(Context.ConnectionId, $"{StreamingType.DATA_EVENT}_{user.Agent}");

            await Clients.All.SendAsync("ReceiveMessage", $"{Context.ConnectionId} has connected to Main");
        }
    }
}
