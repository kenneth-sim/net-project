﻿using net_project.Data;
using net_project.Models.Dto;
using net_project.Models.Entities;
using net_project.Models.Error;
using net_project.Services.Interfaces;
using Newtonsoft.Json;
using Serilog;
using System.Diagnostics;

namespace net_project.Middlewares
{
    // You may need to install the Microsoft.AspNetCore.Http.Abstractions package into your project
    public class LoggingMiddleware
    {
        private readonly RequestDelegate _next;

        public LoggingMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext httpContext, IJwtService jwtService, AppDbContext db)
        {
            AuditLog auditLog = new AuditLog();
            auditLog.Method = httpContext.Request.Method;

            if (!auditLog.Method.Equals("GET"))
            {
                auditLog.RequestTime = DateTime.Now;
                auditLog.RequestData = await GetRequestDataAsync(httpContext.Request) ?? null;
                Stopwatch stopwatch = new Stopwatch();
                var originalResponseBody = httpContext.Response.Body;
                httpContext.Response.Body = new MemoryStream();
                try
                {
                    string? token = httpContext.Request.Headers["Authorization"];
                    if (token != null && !(httpContext.Request.Path.ToString().Contains("login") || httpContext.Request.Path.ToString().Contains("register")))
                    {
                        string? jwtToken = token?.Split(' ')?.Length == 2 ? token?.Split(' ')[1] : null;
                        if (jwtToken != null)
                        {
                            auditLog.Jwt = jwtToken;
                            auditLog.Username = jwtService.ExtractUsername(jwtToken);
                        }
                    }

                    stopwatch.Start();

                    await _next(httpContext);
                }
                catch (Exception ex)
                {
                    httpContext.Response.Body = originalResponseBody;
                    auditLog.ResponseData = ex.Message;
                    httpContext.Response.ContentType = "application/json";
                    httpContext.Response.StatusCode = ex is ErrorCodeException ? StatusCodes.Status400BadRequest : StatusCodes.Status500InternalServerError;
                    await httpContext.Response.WriteAsync(JsonConvert.SerializeObject(new Response(ErrorCode.EXCEPTION_THROWN.GetStatusCode(), ErrorCode.EXCEPTION_THROWN.ToString(), ex.Message)));
                }
                finally
                {
                    stopwatch.Stop();
                    auditLog.ResponseTime = DateTime.Now;

                    if (auditLog.ResponseData == null)
                    {
                        auditLog.ResponseData = await GetResponseBodyAsync(httpContext.Response) ?? null;
                        await httpContext.Response.Body.CopyToAsync(originalResponseBody);
                    }

                    auditLog.ElapsedTime = $"{stopwatch.ElapsedMilliseconds}ms";
                    auditLog.RequestUrl = $"{httpContext.Request.Scheme}://{httpContext.Request.Host}{httpContext.Request.Path}{httpContext.Request.QueryString}";

                    Log.Information("{@AuditLog}", auditLog);
                    db.AuditLogs.Add(auditLog);
                    db.SaveChanges();
                }
            }
            else
            {
                await _next(httpContext);
            }
        }


        private async Task<string> GetRequestDataAsync(HttpRequest request)
        {
            // Ensure the request's body can be read multiple times (for the next middlewares in the pipeline).  
            request.EnableBuffering();

            string requestBody;
            using (var streamReader = new StreamReader(request.Body, leaveOpen: true))
            {
                requestBody = await streamReader.ReadToEndAsync();
                // Reset the request's body stream position for next middleware in the pipeline.  
                request.Body.Position = 0;
            };

            var requestData = new
            {
                requestBody,
                requestQuery = request.QueryString.ToString()
            };

            return JsonConvert.SerializeObject(requestData);
        }

        private async Task<string?> GetResponseBodyAsync(HttpResponse response)
        {
            response.Body.Seek(0, SeekOrigin.Begin);
            string responseBody = await new StreamReader(response.Body).ReadToEndAsync();
            response.Body.Seek(0, SeekOrigin.Begin); // Reset the position for subsequent middleware
            return responseBody;
        }
    }

    // Extension method used to add the middleware to the HTTP request pipeline.
    public static class LoggingMiddlewareExtensions
    {
        public static IApplicationBuilder UseLoggingMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<LoggingMiddleware>();
        }
    }
}
