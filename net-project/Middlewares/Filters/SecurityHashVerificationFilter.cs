﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using net_project.Common.Encryptor;
using net_project.Models.Dto;
using net_project.Models.Error;
using Serilog;

namespace net_project.Middlewares.Filters
{
    public class SecurityHashVerificationFilter : ActionFilterAttribute
    {
        
        public override async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var sign = context.HttpContext.Request.Headers["Sign"].FirstOrDefault();

            //dynamic requestBody;
            //using (StreamReader reader = new StreamReader(context.HttpContext.Request.Body, Encoding.UTF8))
            //{
            //    context.HttpContext.Request.Body.Seek(0, SeekOrigin.Begin);
            //    requestBody = await reader.ReadToEndAsync();
            //    context.HttpContext.Request.Body.Seek(0, SeekOrigin.Begin);
            //}
            //JObject body = JObject.Parse(requestBody);
            //var agentCode = (string)body["Agent"];


            var expectedHash = Md5.CalcMD5Hash("testing").ToLower();

            if (sign != expectedHash)
            {
                Log.Error("Sign Mismatch");
                context.Result = new ObjectResult(new Response(ErrorCode.ERROR_SIGN_MISMATCH.GetStatusCode(), ErrorCode.ERROR_SIGN_MISMATCH.ToString(), ErrorCode.ERROR_SIGN_MISMATCH.GetMessage()));
                return;
            }

            await next();
        }
    }
}
