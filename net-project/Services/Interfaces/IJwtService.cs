﻿using System.Security.Claims;

namespace net_project.Services.Interfaces
{
    public interface IJwtService
    {
        string GenerateJwtToken(string username, IEnumerable<string> roles);
        List<Claim> ExtractClaims(string jwt);
        T ExtractClaim<T>(string jwt, Func<IEnumerable<Claim>, T> claimsResolver);
        string? ExtractUsername(string jwt);
        DateTime? ExtractExpiration(string jwt);
        bool IsJwtExpired(string jwt);
    }
}
