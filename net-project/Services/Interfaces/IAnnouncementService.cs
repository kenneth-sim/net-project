﻿using net_project.Models.Entities;

namespace net_project.Services.Interfaces
{
    public interface IAnnouncementService
    {
        Task<IEnumerable<Announcement>> GetAnnouncements();
        void AddAnnouncement(Announcement newAnnouncement);
        void DeleteAnnouncement(int id);
        void UpdateAnnouncement(Announcement announcement);
        Task<IEnumerable<Announcement>> GetValidAnnouncements();
    }
}
