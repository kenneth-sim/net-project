﻿using AuthService.Models.Dto;
using net_project.Models.Dto;

namespace net_project.Services.Interfaces
{
    public interface IAuthService
    {
        Task Register(RegistrationRequest requestDto);
        Task<string> Login(LoginRequest requestDto);
        void Logout(string username);
        Task AssignRole(AssignRoleRequest assignRoleRequest);
    }
}
