﻿using net_project.Models.Entities;

namespace net_project.Services.Interfaces
{
    public interface IUserService
    {
        User GetUserByUserName(String userName);
    }
}
