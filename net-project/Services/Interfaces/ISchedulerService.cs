﻿namespace net_project.Services.Interfaces
{
    public interface ISchedulerService
    {
        Task SendMessageToCrypto();
        Task SendMessageToStock();
        Task SendMessageToEvent
            ();
    }
}
