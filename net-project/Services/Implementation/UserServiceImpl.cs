﻿using net_project.Data;
using net_project.Models.Dto;
using net_project.Models.Entities;
using net_project.Models.Error;
using net_project.Services.Interfaces;

namespace net_project.Services.Implementation
{
    public class UserServiceImpl : IUserService
    {
        private readonly AppDbContext _db;

        public UserServiceImpl(AppDbContext db)
        {
            _db = db;
        }

        public User GetUserByUserName(string userName)
        {
            var user = _db.Users.FirstOrDefault(u => u.UserName.Equals(userName));

            if (user == null)
            {
                throw new ErrorCodeException(ErrorCode.NOTFOUND_USER);
            }

            return user;
        }
    }
}
