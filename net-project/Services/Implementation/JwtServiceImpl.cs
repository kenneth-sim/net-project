﻿using net_project.Services.Interfaces;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace net_project.Services.Implementation
{
    public class JwtServiceImpl : IJwtService
    {
        private readonly IConfiguration _configuration;

        public JwtServiceImpl(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public string GenerateJwtToken(string username, IEnumerable<string> roles)
        {
            List<Claim> claims = new List<Claim>{
                new Claim(ClaimTypes.Name, username)
            };

            claims.AddRange(roles.Select(role => new Claim(ClaimTypes.Role, role)));

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration.GetSection("AppSettings:JWT_KEY").Value!));
            var token = new JwtSecurityToken(
                    claims: claims,
                    expires: DateTime.Now.AddDays(1),
                    signingCredentials: new SigningCredentials(key, SecurityAlgorithms.HmacSha256Signature)
                );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        public List<Claim> ExtractClaims(string jwt)
        {
            var securityToken = new JwtSecurityTokenHandler().ReadToken(jwt) as JwtSecurityToken;
            if (securityToken != null)
            {
                return securityToken.Claims.ToList();
            }
            return new List<Claim>();
        }

        public T ExtractClaim<T>(string jwt, Func<IEnumerable<Claim>, T> claimsResolver)
        {
            return claimsResolver(ExtractClaims(jwt));
        }

        public string? ExtractUsername(string jwt)
        {
            return ExtractClaim(jwt, claims => claims.FirstOrDefault(c => c.Type == ClaimTypes.Name)?.Value);
        }

        public DateTime? ExtractExpiration(string jwt)
        {
            var expirationValue = ExtractClaim(jwt, claims => claims.FirstOrDefault(c => c.Type == JwtRegisteredClaimNames.Exp)?.Value);

            if (expirationValue != null && long.TryParse(expirationValue, out long expirationTime))
            {
                return DateTimeOffset.FromUnixTimeSeconds(expirationTime).UtcDateTime;
            }

            return null;
        }

        public bool IsJwtExpired(string jwt)
        {
            var dateTime = ExtractExpiration(jwt);

            if (dateTime.HasValue)
            {
                return DateTime.Compare((DateTime)dateTime, DateTime.Now) < 0;
            }

            return true;
        }
    }
}
