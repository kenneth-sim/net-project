﻿using AuthService.Models.Dto;
using net_project.Data;
using net_project.Models;
using net_project.Models.Dto;
using net_project.Models.Entities;
using net_project.Models.Error;
using net_project.Services.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Caching.Distributed;

namespace net_project.Services.Implementation
{
    public class AuthServiceImpl : BaseServiceImpl, IAuthService
    {
        private readonly IJwtService _jwtService;
        private readonly AppDbContext _db;
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;

        public AuthServiceImpl(IJwtService jwtService, 
                                AppDbContext db, 
                                UserManager<User> userManager, 
                                RoleManager<IdentityRole> roleManager, 
                                IDistributedCache distributedCache) : base(distributedCache, db)
        {
            _jwtService = jwtService;
            _db = db;
            _userManager = userManager;
            _roleManager = roleManager;
        }

        public async Task AssignRole(AssignRoleRequest assignRoleRequest)
        {
            var user = _db.Users.FirstOrDefault(u => u.UserName.Equals(assignRoleRequest.Username));

            if (user == null)
            {
                throw new ErrorCodeException(ErrorCode.NOTFOUND_USER);
            }

            if (!_roleManager.RoleExistsAsync(assignRoleRequest.RoleName).GetAwaiter().GetResult())
            {
                assignRoleRequest.RoleName = SD.RoleType.ROLE_PLAYER.ToString();
            }

            await _userManager.AddToRoleAsync(user, assignRoleRequest.RoleName);
        }

        public async Task<string> Login(LoginRequest requestDto)
        {
            var user = _db.Users.FirstOrDefault(u => u.UserName.Equals(requestDto.Username));

            if (user == null)
            {
                throw new ErrorCodeException(ErrorCode.NOTFOUND_USER);
            }

            bool isValid = await _userManager.CheckPasswordAsync(user, requestDto.Password);

            if (!isValid)
            {
                throw new ErrorCodeException(ErrorCode.ERROR_INVALID_PASSWORD);
            }

            var roles = await _userManager.GetRolesAsync(user);

            string jwt = _jwtService.GenerateJwtToken(user.UserName, roles);

            return jwt;
        }

        public void Logout(string username)
        {
            var user = _db.Users.FirstOrDefault(u => u.UserName.Equals(username));

            if (user == null)
            {
                throw new ErrorCodeException(ErrorCode.NOTFOUND_USER);
            }

            user.Jwt = null;
            _db.SaveChanges();
        }

        public async Task Register(RegistrationRequest requestDto)
        {
            User user = new()
            {
                UserName = requestDto.Username,
                Email = requestDto.Email,
                Agent = requestDto.Agent,
                Name = requestDto.Name,
                PhoneNumber = requestDto.PhoneNumber
            };

            var result = await _userManager.CreateAsync(user, requestDto.Password);

            if (!result.Succeeded)
            {
                throw new ErrorCodeException(ErrorCode.ERROR_REGISTERED_FAILED, result.Errors.FirstOrDefault()?.Description ?? "");
            }

            await AssignRole(new AssignRoleRequest
            {
                Username = requestDto.Username,
                RoleName = requestDto.RoleName ?? SD.RoleType.ROLE_PLAYER.ToString()
            });
        }
    }
}
