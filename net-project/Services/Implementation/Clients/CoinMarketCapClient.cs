﻿using Newtonsoft.Json.Linq;
using System.Web;

namespace net_project.Services.Implementation.Clients
{
    public class CoinMarketCapClient
    {
        private const string API_KEY = "c5984c59-5e7a-4191-89f3-76c2ece0ea40";
        private readonly HttpClient _httpClient = new HttpClient()
        {
            BaseAddress = new Uri("https://pro-api.coinmarketcap.com/v1/")
        };

        public async Task<IEnumerable<CryptoInfo>> GetTop5LatestCrypto()
        {
            List<CryptoInfo> cryptoList = new List<CryptoInfo>();
            var queryString = HttpUtility.ParseQueryString(string.Empty);
            queryString["start"] = "1";
            queryString["limit"] = "5";
            queryString["convert"] = "USD";

            _httpClient.DefaultRequestHeaders.Add("X-CMC_PRO_API_KEY", API_KEY);
            _httpClient.DefaultRequestHeaders.Add("Accepts", "application/json");

            var response = await _httpClient.GetAsync("cryptocurrency/listings/latest?" + queryString);
            if (response.IsSuccessStatusCode)
            {
                var result = await response.Content.ReadAsStringAsync();

                JObject json = JObject.Parse(result);
                foreach (var crypto in json["data"]!)
                {
                    cryptoList.Add(new CryptoInfo
                    {
                        Name = crypto["name"].ToString(),
                        PriceUSD = Convert.ToDouble(crypto["quote"]["USD"]["price"])
                    });
                }
                return cryptoList;

            }
            return cryptoList;
        }

        public class CryptoInfo
        {
            public string Name { get; set; }
            public double PriceUSD { get; set; }
        }
    }
}
