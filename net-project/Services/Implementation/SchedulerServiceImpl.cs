﻿using net_project.Hubs;
using net_project.Models;
using net_project.Services.Interfaces;
using Microsoft.AspNetCore.SignalR;
using static net_project.Models.SD;

namespace net_project.Services.Implementation
{
    public class SchedulerServiceImpl : ISchedulerService
    {
        public readonly IHubContext<StreamingHub> _hubContext;

        public SchedulerServiceImpl(IHubContext<StreamingHub> hubContext)
        {
            _hubContext = hubContext;
        }

        public async Task SendMessageToCrypto()
        {
            Random _random = new Random();
            var ok = _random.Next(1000, 5000 + 1).ToString();
            await SendMessage(ok, StreamingType.DATA_CRYPTO);
        }

        public async Task SendMessageToEvent()
        {
            Random _random = new Random();
            var ok = _random.Next(1000, 5000 + 1).ToString();
            await SendMessage(ok, StreamingType.DATA_EVENT, "TEST");
        }

        public async Task SendMessageToStock()
        {
            Random _random = new Random();
            var ok = _random.Next(1000, 5000 + 1).ToString();
            await SendMessage(ok, StreamingType.DATA_STOCK);
        }

        private async Task SendMessage(string message, StreamingType streamingType, string agent = null)
        {
            if (streamingType.Equals(StreamingType.DATA_EVENT))
            {
                await _hubContext.Clients.Group($"{streamingType}_{agent}").SendAsync($"{streamingType}", message);
            }
            else
            {
                await _hubContext.Clients.All.SendAsync($"{streamingType}", message);
            }
        }
    }
}
