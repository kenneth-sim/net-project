﻿using net_project.Data;
using net_project.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Diagnostics;

namespace net_project.Interceptors
{
    public sealed class AuditableEntityInterceptor : SaveChangesInterceptor
    {
        public override InterceptionResult<int> SavingChanges(DbContextEventData eventData, InterceptionResult<int> result)
        {
            var db = eventData.Context;

            if (db == null)
            {
                return base.SavingChanges(eventData, result);
            }

            SavedChangesHandler((AppDbContext)db);

            return base.SavingChanges(eventData, result);
        }

        public override ValueTask<InterceptionResult<int>> SavingChangesAsync(DbContextEventData eventData, InterceptionResult<int> result, CancellationToken cancellationToken = default)
        {
            var db = eventData.Context;

            if (db == null)
            {
                return base.SavingChangesAsync(eventData, result, cancellationToken);
            }

            SavedChangesHandler((AppDbContext)db);

            return base.SavingChangesAsync(eventData, result, cancellationToken);
        }

        private void SavedChangesHandler(AppDbContext db)
        {
            IEnumerable<EntityEntry<IAuditableEntity>> entries = db.ChangeTracker.Entries<IAuditableEntity>();

            foreach (EntityEntry<IAuditableEntity> entry in entries)
            {
                if (entry.State == EntityState.Added)
                {
                    entry.Property(a => a.CreatedAt).CurrentValue = DateTime.UtcNow;
                }

                if (entry.State == EntityState.Modified)
                {
                    entry.Property(a => a.UpdatedAt).CurrentValue = DateTime.UtcNow;
                }

            }
        }
    }
}
